# gtk-nlp-finance-vanguard-go

a Natural Language Processing (nlp) API specifically for financial [Vanguard](https://about.vanguard.com/) words and phrases. it's used to text mine and get categorizations, with weight, back. This is based on the original app that I did in ASP.NET Core 2.2 (C#) [https://gitlab.com/gkama/gtk-nlp-finance-vanguard](here). it's simply that functionality (and a little extra) in Go

## examples

`/categorize` example with a sample request and response

```json
{
    "content": "this is a test to find VBISX, VBISX, VEXMX and VEXMX Vanguard index funds"
}
```

```json
[
    {
        "category": "Index Funds",
        "total_weight": 4,
        "matched": [
            {
                "value": "VBISX",
                "weight": 2
            },
            {
                "value": "VEXMX",
                "weight": 2
            }
        ]
    }
]
```
